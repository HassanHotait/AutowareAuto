# Copyright 2020 The Autoware Foundation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import ament_index_python
import launch
import launch_ros.actions
from launch_ros.actions import Node
from launch import LaunchDescription

n_robots=1




# def generate_launch_description():
#     """Launch recordreplay_planner_node with default configuration."""
#     # -------------------------------- Nodes-----------------------------------
#     recordreplay_planner_node = launch_ros.actions.Node(
#         package='recordreplay_planner_nodes',
#         node_executable='recordreplay_planner_node_exe',
#         node_name='recordreplay_planner',
#         node_namespace='/Robot0',
#         output='screen',
#         parameters=[
#             "{}/param/defaults_copied.param.yaml".format(
#                 ament_index_python.get_package_share_directory(
#                     "recordreplay_planner_nodes"
#                 )
#             ),
#         ],
#         remappings=[
#             ('vehicle_state', '/Robot0/vehicle_kinematic_state'),
#             ('planned_trajectory', '/trajectory'),
#             ('obstacle_bounding_boxes', '/Robot0/perception/lidar_bounding_boxes'),

#         ]
#     )

#     ld = launch.LaunchDescription([
#         recordreplay_planner_node]
#     )
#     return ld

def generate_launch_description():

    nodes=[]

    for i in range(n_robots):

        state_remapped="/Robot"+str(i)+"/vehicle_kinematic_state"
        trajectory_remapped="/Robot"+str(i)+"/trajectory"
        robot_namespace='/Robot'+str(i)
        tf_remapped="/Robot"+str(i)+"/tf"
        tf_static_remapped="/Robot"+str(i)+"/tf_static"


        recordreplay_planner_node = Node(
        package='recordreplay_planner_nodes',
        node_executable='recordreplay_planner_node_exe',
        node_name='recordreplay_planner',
        node_namespace=robot_namespace,
        output='screen',
        parameters=[
            "{}/param/defaults_copied.param.yaml".format(
                ament_index_python.get_package_share_directory(
                    "recordreplay_planner_nodes"
                )
            ),
        ],
        remappings=[
            ('vehicle_state', state_remapped),
            ('planned_trajectory', trajectory_remapped),
            ("/tf_static",tf_static_remapped),
            ("/tf",tf_remapped)

        ]
        )

        nodes.append(recordreplay_planner_node)


    return LaunchDescription(nodes)

