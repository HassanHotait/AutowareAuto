#
#Copyright 2020-2021 The Autoware Foundation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Testing of purepursuit_controller in LGSVL simulation using recordreplay planner."""

from sympy import E
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python import get_package_share_directory
import os


n_robots=1

trajectory_replay_mode=False




def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)


def generate_launch_description():
    
    """
    Launch nodes with params to test record_replay_planner with pure pursuit in simulation.

    pure_pursuit_controller + LGSVL + recordreplay planner + lidar osbtacle ddetection
    """
    # --------------------------------- Params -------------------------------
    pure_pursuit_controller_param_file = get_share_file(
        package_name='test_trajectory_following',
        file_name='param/my_pure_pursuit_controller.param.yaml')
    rviz_cfg_path = get_share_file(
        package_name='test_trajectory_following', file_name='config/default_control.rviz')
    urdf_path = get_share_file(
        package_name='lexus_rx_450h_description', file_name='urdf/lexus_rx_450h.urdf')

    # --------------------------------- Arguments -------------------------------
    pure_pursuit_controller_param = DeclareLaunchArgument(
        'pure_pursuit_controller_param_file',
        default_value=pure_pursuit_controller_param_file,
        description='Path to config file for Pure Pursuit Controller'
    )
    with_pure_pursuit_param = DeclareLaunchArgument(
        'with_pure_pursuit',
        default_value='True',
        description='Enable pure pursuit controller'
    )
    with_obstacle_param = DeclareLaunchArgument(
        'with_obstacle',
        default_value='False',
        description='Enable obstacle detection stack \
            (filter_transform + ray_ground_classifier + euclidean_clustering)'
    )

    # # -------------------------------- Nodes-----------------------------------
    freespace_planner_path = get_share_file(
        package_name='freespace_planner_nodes',
        file_name='launch/myfreespace_planner.launch.py'),
    freespace_planner_node = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(freespace_planner_path)
        


    )

    recordreplay_planner_path = get_share_file(
        package_name='recordreplay_planner_nodes',
        file_name='launch/myrecordreplay_planner_node.launch.py'),
    recordreplay_planner_node = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(recordreplay_planner_path) 

    )

    description=[with_pure_pursuit_param,pure_pursuit_controller_param]


    nodes=[]



    nodes.append(recordreplay_planner_node)
    
    nodes.append(freespace_planner_node)


    for i in range(n_robots):
        purepursuit_command_remapped="/Robot"+str(i)+"/vehicle_command_purepursuit"
        trajectory_remapped="/Robot"+str(i)+"/trajectory"
        trajectory_transformed_remapped="/Robot"+str(i)+"/trajectory_transformed"
        state_remapped="/Robot"+str(i)+"/vehicle_kinematic_state"
        diagnostic_remapped="/Robot"+str(i)+"/control_diagnostic"
        robot_namespace='/Robot'+str(i)
        tf_remapped="/Robot"+str(i)+"/tf"
        tf_static_remapped="/Robot"+str(i)+"/tf_static"

        pure_pursuit_controller_node = Node(
            package="pure_pursuit_nodes",
            node_executable="pure_pursuit_node_exe",
            node_name="pure_pursuit_controller",
            node_namespace=robot_namespace,
            parameters=[LaunchConfiguration('pure_pursuit_controller_param_file')],
            output='screen',
            remappings=[
                ("current_pose", state_remapped),
                ("trajectory", trajectory_remapped),
                ("ctrl_cmd", purepursuit_command_remapped),
                ("ctrl_diag", diagnostic_remapped),
                ("static_tf",tf_static_remapped),
                ("tf",tf_remapped)

            ],
            condition=IfCondition(LaunchConfiguration('with_pure_pursuit'))
        )

    #     global_planner = Node(
    #     package='lanelet2_global_planner_nodes',
    #     node_name='lanelet2_global_planner_node',
    #     output='screen',
    #     #node_namespace='Robot0',
    #     node_executable='lanelet2_global_planner_node_exe',
    #     remappings=[('HAD_Map_Client', '/had_maps/HAD_Map_Service'),
    #                 ('vehicle_kinematic_state', state_remapped),
    #                 ('tf', tf_remapped),
    #                 ('tf_static', tf_static_remapped)]
    # )

        rviz2 = Node(
            package='rviz2',
            node_executable='rviz2',
            output='log',
            node_name='rviz2',
            arguments=['-d', str(rviz_cfg_path)],
            remappings=[
            ('/tf', tf_remapped),
            ('/tf_static', tf_static_remapped)]
    
            
        )
        nodes.append(pure_pursuit_controller_node)
        nodes.append(rviz2)
   # urdf_publisher = Node(
    #    package='robot_state_publisher',
     #   node_executable='robot_state_publisher',
      #  node_name='robot_state_publisher',
       # arguments=[str(urdf_path)]

    
   # )
    description.extend(nodes)
    return LaunchDescription(description)

        # nodes
        #pure_pursuit_controller_node,
        #urdf_publisher,
        #rviz2
    # ])
