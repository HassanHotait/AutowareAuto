#
#Copyright 2020-2021 The Autoware Foundation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Testing of purepursuit_controller in LGSVL simulation using recordreplay planner."""

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python import get_package_share_directory
import os


def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)


def generate_launch_description():
    """
    Launch nodes with params to test record_replay_planner with pure pursuit in simulation.

    pure_pursuit_controller + LGSVL + recordreplay planner + lidar osbtacle ddetection
    """
    # --------------------------------- Params -------------------------------
    pure_pursuit_controller_param_file = get_share_file(
        package_name='test_trajectory_following',
        file_name='param/my_pure_pursuit_controller.param.yaml')
    rviz_cfg_path = get_share_file(
        package_name='test_trajectory_following', file_name='config/default_control.rviz')
    urdf_path = get_share_file(
        package_name='lexus_rx_450h_description', file_name='urdf/lexus_rx_450h.urdf')


    mpc_controller_param_file = get_share_file(
        package_name='test_trajectory_following', file_name='param/my_mpc_controller.param.yaml')

    # --------------------------------- Arguments -------------------------------
    
    mpc_controller_param = DeclareLaunchArgument(
    'mpc_controller_param_file',
    default_value=mpc_controller_param_file,
    description='Path to config file for MPC Controller'
    )






    with_obstacle_param = DeclareLaunchArgument(
        'with_obstacle',
        default_value='False',
        description='Enable obstacle detection stack \
            (filter_transform + ray_ground_classifier + euclidean_clustering)'
    )

    # -------------------------------- Nodes-----------------------------------
    recordreplay_planner_path = get_share_file(
        package_name='recordreplay_planner_nodes',
        file_name='launch/myrecordreplay_planner_node.launch.py'),
    recordreplay_planner_node = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(recordreplay_planner_path)
        


    )

    mpc_controller_nodes = Node(
        package="mpc_controller_nodes",
        node_executable="mpc_controller_node_exe",
        node_name="mpc_controller",
        #node_namespace='control',
        parameters=[LaunchConfiguration('mpc_controller_param_file')],
        output='screen',
        remappings=[
        ("/vehicle/vehicle_kinematic_state", "/Robot0/vehicle_kinematic_state"),
        ("/vehicle/vehicle_command", "/Robot0/vehicle_command")
        #("ctrl_diag", "/Robot0/control_diagnostic"),
        #("static_tf","/tf_static")
        ]
        )


    rviz2 = Node(
        package='rviz2',
        node_executable='rviz2',
        node_name='rviz2',
        arguments=['-d', str(rviz_cfg_path)]
    )
   # urdf_publisher = Node(
    #    package='robot_state_publisher',
     #   node_executable='robot_state_publisher',
      #  node_name='robot_state_publisher',
       # arguments=[str(urdf_path)]
   # )
    return LaunchDescription([
        recordreplay_planner_node,
        mpc_controller_param,
        mpc_controller_nodes,

        #urdf_publisher,
        rviz2
    ])
